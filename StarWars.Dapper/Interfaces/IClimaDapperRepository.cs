﻿using StarWars.Domain.Core.Repository_Interfaces;

namespace StarWars.DataAccess.EntityFramework.Interfaces
{
    public interface IClimaDapperRepository : IClimaRepository
    {
    }
}
