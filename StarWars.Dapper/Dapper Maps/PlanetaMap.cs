﻿using Dapper.FluentMap.Mapping;
using StarWars.Domain.Core.Domain_Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace StarWars.DataAccess.Dapper.Dapper_Maps
{
    public class PlanetaMap : EntityMap<Planeta>
    {
        public PlanetaMap()
        {
            Map(p => p.Id).ToColumn("SAMPLE_ID");
        }
    }
}