﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using StarWars.DataAccess.EntityFramework.Interfaces;
using StarWars.Domain.Core.Domain_Entities;

namespace StarWars.DataAccess.Dapper.Repositories
{
    public class PlanetaDapperRepository : DapperRepositoryBase<Planeta>, IPlanetaDapperRepository
    {
        public PlanetaDapperRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public override void Add(Planeta obj)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<Planeta> GetAll()
        {
            throw new NotImplementedException();
        }

        public override Planeta GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public Planeta GetByName(string name)
        {
            throw new NotImplementedException();
        }

        public override void Remove(Planeta obj)
        {
            throw new NotImplementedException();
        }

        public override void Update(Planeta obj)
        {
            throw new NotImplementedException();
        }
    }
}
