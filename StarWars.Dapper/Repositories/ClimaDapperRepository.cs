﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using StarWars.DataAccess.EntityFramework.Interfaces;
using StarWars.Domain.Core.Domain_Entities;

namespace StarWars.DataAccess.Dapper.Repositories
{
    public class ClimaDapperRepository : DapperRepositoryBase<Clima>, IClimaDapperRepository
    {
        public ClimaDapperRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public override void Add(Clima obj)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<Clima> GetAll()
        {
            throw new NotImplementedException();
        }

        public override Clima GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public override void Remove(Clima obj)
        {
            throw new NotImplementedException();
        }

        public override void Update(Clima obj)
        {
            throw new NotImplementedException();
        }
    }
}
