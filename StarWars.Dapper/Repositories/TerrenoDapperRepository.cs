﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using StarWars.DataAccess.EntityFramework.Interfaces;
using StarWars.Domain.Core.Domain_Entities;

namespace StarWars.DataAccess.Dapper.Repositories
{
    public class TerrenoDapperRepository : DapperRepositoryBase<Terreno>, ITerrenoDapperRepository
    {
        public TerrenoDapperRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public override void Add(Terreno obj)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<Terreno> GetAll()
        {
            throw new NotImplementedException();
        }

        public override Terreno GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public override void Remove(Terreno obj)
        {
            throw new NotImplementedException();
        }

        public override void Update(Terreno obj)
        {
            throw new NotImplementedException();
        }
    }
}
