﻿using Dapper.FluentMap;
using StarWars.DataAccess.Dapper.Dapper_Maps;

namespace StarWars.RegistrationTools
{
    public class DapperRegistration
    {
        public static void RegisterMaps()
        {
            #region Value Objects
            #endregion

            #region Entities
            FluentMapper.Initialize(config => config.AddMap(new PlanetaMap()));
            #endregion
        }
    }
}
