﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StarWars.Application.Application_Services_Interfaces;
using StarWars.Domain.Core.Domain_Entities;

namespace StarWars.Presentation.Controllers
{
    public class ClimaController : Controller
    {
        private IClimaApplicationService _climaApplicationService;

        public ClimaController(IClimaApplicationService climaApplicationService)
        {
            _climaApplicationService = climaApplicationService;
        }

        public ActionResult Index()
        {
            var climas = _climaApplicationService.GetAll();
            return View(climas);
        }

        public ActionResult Details(Guid id)
        {
            var clima = _climaApplicationService.GetById(id);
            return View(clima);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Clima clima)
        {
            try
            {
                _climaApplicationService.Add(clima);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit(Guid id)
        {
            var clima = _climaApplicationService.GetById(id);
            return View(clima);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Clima clima)
        {
            try
            {
                _climaApplicationService.Update(clima);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(Guid id)
        {
            var clima = _climaApplicationService.GetById(id);
            return View(clima);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Guid id, IFormCollection collection)
        {
            try
            {
                var clima = _climaApplicationService.GetById(id);
                _climaApplicationService.Remove(clima);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}