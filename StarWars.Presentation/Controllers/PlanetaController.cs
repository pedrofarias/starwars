﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StarWars.Application.Application_Services_Interfaces;
using StarWars.Domain.Core.Domain_Entities;

namespace StarWars.Presentation.Controllers
{
    public class PlanetaController : Controller
    {
        private IPlanetaApplicationService _planetaApplicationService;
        private IClimaApplicationService _climaApplicationService;
        private ITerrenoApplicationService _terrenoApplicationService;

        public PlanetaController(
            IPlanetaApplicationService planetaApplicationService,
            IClimaApplicationService climaApplicationService,
            ITerrenoApplicationService terrenoApplicationService)
        {
            _planetaApplicationService = planetaApplicationService;
            _terrenoApplicationService = terrenoApplicationService;
            _climaApplicationService = climaApplicationService;
        }

        public ActionResult Index()
        {
            var planetas = _planetaApplicationService.GetAll();
            return View(planetas);
        }

        public ActionResult Details(Guid id)
        {
            var planeta = _planetaApplicationService.GetById(id);
            return View(planeta);
        }

        public ActionResult Create()
        {
            ViewBag.Terrenos = _terrenoApplicationService.GetAll();
            ViewBag.Climas = _climaApplicationService.GetAll();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Planeta planeta)
        {
            try
            {
                planeta.Terreno = _terrenoApplicationService.GetById(planeta.Terreno.Id);
                planeta.Clima = _climaApplicationService.GetById(planeta.Clima.Id);
                _planetaApplicationService.Add(planeta);
                return RedirectToAction(nameof(Index));
            }
            catch(Exception e)
            {
                return View(e);
            }
        }

        public ActionResult Edit(Guid id)
        {
            ViewBag.Terrenos = _terrenoApplicationService.GetAll();
            ViewBag.Climas = _climaApplicationService.GetAll();
            var planeta = _planetaApplicationService.GetById(id);
            return View(planeta);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Planeta planeta)
        {
            try
            {
                planeta.Terreno = _terrenoApplicationService.GetById(planeta.Terreno.Id);
                planeta.Clima = _climaApplicationService.GetById(planeta.Clima.Id);
                _planetaApplicationService.Update(planeta);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(Guid id)
        {
            var planeta = _planetaApplicationService.GetById(id);
            return View(planeta);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Guid id, IFormCollection collection)
        {
            try
            {
                var planeta = _planetaApplicationService.GetById(id);
                _planetaApplicationService.Remove(planeta);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}