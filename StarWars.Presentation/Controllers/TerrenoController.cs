﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StarWars.Application.Application_Services_Interfaces;
using StarWars.Domain.Core.Domain_Entities;

namespace StarWars.Presentation.Controllers
{
    public class TerrenoController : Controller
    {
        private ITerrenoApplicationService _terrenoApplicationService;

        public TerrenoController(ITerrenoApplicationService terrenoApplicationService)
        {
            _terrenoApplicationService = terrenoApplicationService;
        }

        public ActionResult Index()
        {
            var terrenos = _terrenoApplicationService.GetAll();
            return View(terrenos);
        }

        public ActionResult Details(Guid id)
        {
            var terreno = _terrenoApplicationService.GetById(id);
            return View(terreno);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Terreno terreno)
        {
            try
            {
                _terrenoApplicationService.Add(terreno);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit(Guid id)
        {
            var terreno = _terrenoApplicationService.GetById(id);
            return View(terreno);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Terreno terreno)
        {
            try
            {
                _terrenoApplicationService.Update(terreno);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(Guid id)
        {
            var terreno = _terrenoApplicationService.GetById(id);
            return View(terreno);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Guid id, IFormCollection collection)
        {
            try
            {
                var terreno = _terrenoApplicationService.GetById(id);
                _terrenoApplicationService.Remove(terreno);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}