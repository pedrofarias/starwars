﻿using StarWars.Domain.Core.Domain_Entities;
using StarWars.Domain.Core.Repository_Interfaces;
using StarWars.Domain.Domain_Services_Interfaces;

namespace StarWars.Domain.Domain_Services
{
    public class ClimaService : ServiceBase<Clima>, IClimaService
    {
        private readonly IClimaRepository _climaRepository;
        public ClimaService(IClimaRepository climaRepository) : base(climaRepository)
        {
            _climaRepository = climaRepository;
        }
    }
}
