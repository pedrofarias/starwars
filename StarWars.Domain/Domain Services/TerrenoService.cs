﻿using StarWars.Domain.Core.Domain_Entities;
using StarWars.Domain.Core.Repository_Interfaces;
using StarWars.Domain.Domain_Services_Interfaces;

namespace StarWars.Domain.Domain_Services
{
    public class TerrenoService : ServiceBase<Terreno>, ITerrenoService
    {
        private readonly ITerrenoRepository _terrenoRepository;
        public TerrenoService(ITerrenoRepository terrenoRepository) : base(terrenoRepository)
        {
            _terrenoRepository = terrenoRepository;
        }
    }
}
