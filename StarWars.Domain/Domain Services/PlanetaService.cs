﻿using StarWars.Domain.Core.Domain_Entities;
using StarWars.Domain.Core.Repository_Interfaces;
using StarWars.Domain.Domain_Services_Interfaces;

namespace StarWars.Domain.Domain_Services
{
    public class PlanetaService : ServiceBase<Planeta>, IPlanetaService
    {
        private readonly IPlanetaRepository _planetaRepository;
        public PlanetaService(IPlanetaRepository planetaRepository) : base(planetaRepository)
        {
            _planetaRepository = planetaRepository;
        }

        public Planeta GetByName(string name)
        {
            return _planetaRepository.GetByName(name);
        }
    }
}
