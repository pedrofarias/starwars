﻿using StarWars.Domain.Core.Domain_Entities;

namespace StarWars.Domain.Domain_Services_Interfaces
{
    public interface ITerrenoService : IServiceBase<Terreno>
    {
    }
}
