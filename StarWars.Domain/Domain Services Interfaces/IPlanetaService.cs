﻿using StarWars.Domain.Core.Domain_Entities;

namespace StarWars.Domain.Domain_Services_Interfaces
{
    public interface IPlanetaService : IServiceBase<Planeta>
    {
        Planeta GetByName(string name);
    }
}
