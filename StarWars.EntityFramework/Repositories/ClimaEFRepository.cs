﻿using StarWars.DataAccess.EntityFramework.Context;
using StarWars.DataAccess.EntityFramework.Interfaces;
using StarWars.Domain.Core.Domain_Entities;

namespace StarWars.DataAccess.EntityFramework.Repositories
{
    public class ClimaEFRepository : EFRepositoryBase<Clima>, IClimaEFRepository
    {
        public ClimaEFRepository(EntityFrameworkContext context) : base(context)
        {
        }
    }
}
