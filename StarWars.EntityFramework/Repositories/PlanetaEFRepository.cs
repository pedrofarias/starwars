﻿using Microsoft.EntityFrameworkCore;
using StarWars.DataAccess.EntityFramework.Context;
using StarWars.DataAccess.EntityFramework.Interfaces;
using StarWars.Domain.Core.Domain_Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StarWars.DataAccess.EntityFramework.Repositories
{
    public class PlanetaEFRepository : EFRepositoryBase<Planeta>, IPlanetaEFRepository
    {
        public PlanetaEFRepository(EntityFrameworkContext context) : base(context)
        {
        }

        public new IEnumerable<Planeta> GetAll()
        {
            return _context.Planeta
                .Include(planeta => planeta.Clima)
                .Include(planeta => planeta.Terreno)
                .ToList();
        }

        public new Planeta GetById(Guid id)
        {
            return _context.Planeta
                .Include(planeta => planeta.Clima)
                .Include(planeta => planeta.Terreno)
                .SingleOrDefault(p => p.Id == id);
        }

        public Planeta GetByName(string name)
        {
            return _context.Planeta
                .Include(planeta => planeta.Clima)
                .Include(planeta => planeta.Terreno)
                .SingleOrDefault(p => p.Nome.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
