﻿using StarWars.DataAccess.EntityFramework.Context;
using StarWars.DataAccess.EntityFramework.Interfaces;
using StarWars.Domain.Core.Domain_Entities;

namespace StarWars.DataAccess.EntityFramework.Repositories
{
    public class TerrenoEFRepository : EFRepositoryBase<Terreno>, ITerrenoEFRepository
    {
        public TerrenoEFRepository(EntityFrameworkContext context) : base(context)
        {
        }
    }
}
