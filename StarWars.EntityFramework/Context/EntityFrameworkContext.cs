﻿using Microsoft.EntityFrameworkCore;
using StarWars.Domain.Core.Domain_Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StarWars.DataAccess.EntityFramework.Context
{
    public class EntityFrameworkContext : DbContext
    {
        public EntityFrameworkContext(DbContextOptions<EntityFrameworkContext> options) : base(options)
        {
        }

        public DbSet<Planeta> Planeta { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}
