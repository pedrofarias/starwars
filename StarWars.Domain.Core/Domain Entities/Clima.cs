﻿using StarWars.Domain.Core.Shared;
using System;
using System.Collections.Generic;

namespace StarWars.Domain.Core.Domain_Entities
{
    public class Clima : Entity
    {
        public string Nome { get; set; }
    }
}
