﻿using StarWars.Domain.Core.Shared;

namespace StarWars.Domain.Core.Domain_Entities
{
    public class Planeta : Entity
    {
        public string Nome { get; set; }
		
		public Clima Clima { get; set; }
		
		public Terreno Terreno { get; set; }
    }
}
