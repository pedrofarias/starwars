﻿using StarWars.Domain.Core.Domain_Entities;

namespace StarWars.Domain.Core.Repository_Interfaces
{
    public interface IClimaRepository : IRepositoryBase<Clima>
    {
    }
}
