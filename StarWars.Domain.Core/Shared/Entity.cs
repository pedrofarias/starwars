﻿using System;

namespace StarWars.Domain.Core.Shared
{
    public class Entity
    {
        public Guid Id { get; set; } = new Guid();
    }
}
