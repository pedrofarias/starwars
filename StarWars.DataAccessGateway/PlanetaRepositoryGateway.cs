﻿using System;
using System.Collections.Generic;
using StarWars.DataAccess.EntityFramework.Interfaces;
using StarWars.Domain.Core.Domain_Entities;
using StarWars.Domain.Core.Repository_Interfaces;

namespace StarWars.DataAccessGateway
{
    public class PlanetaRepositoryGateway : IPlanetaRepository
    {
        private readonly IPlanetaDapperRepository _dapper;
        private readonly IPlanetaEFRepository _entity;

        public PlanetaRepositoryGateway(IPlanetaDapperRepository dapper, IPlanetaEFRepository entity)
        {
            _dapper = dapper;
            _entity = entity;
        }

        public void Add(Planeta obj) => _entity.Add(obj);

        public void Dispose() => _entity.Dispose();

        public IEnumerable<Planeta> GetAll() => _entity.GetAll();

        public Planeta GetById(Guid id) => _entity.GetById(id);

        public Planeta GetByName(string name) => _entity.GetByName(name);

        public void Remove(Planeta obj) => _entity.Remove(obj);

        public void Update(Planeta obj) => _entity.Update(obj);
    }
}