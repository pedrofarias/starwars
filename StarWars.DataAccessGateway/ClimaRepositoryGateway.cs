﻿using System;
using System.Collections.Generic;
using StarWars.DataAccess.EntityFramework.Interfaces;
using StarWars.Domain.Core.Domain_Entities;
using StarWars.Domain.Core.Repository_Interfaces;

namespace StarWars.DataAccessGateway
{
    public class ClimaRepositoryGateway : IClimaRepository
    {
        private readonly IClimaDapperRepository _dapper;
        private readonly IClimaEFRepository _entity;

        public ClimaRepositoryGateway(IClimaDapperRepository dapper, IClimaEFRepository entity)
        {
            _dapper = dapper;
            _entity = entity;
        }

        public void Add(Clima obj) => _entity.Add(obj);

        public void Dispose() => _entity.Dispose();

        public IEnumerable<Clima> GetAll() => _entity.GetAll();

        public Clima GetById(Guid id) => _entity.GetById(id);

        public void Remove(Clima obj) => _entity.Remove(obj);

        public void Update(Clima obj) => _entity.Update(obj);
    }
}