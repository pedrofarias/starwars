﻿using System;
using System.Collections.Generic;
using StarWars.DataAccess.EntityFramework.Interfaces;
using StarWars.Domain.Core.Domain_Entities;
using StarWars.Domain.Core.Repository_Interfaces;

namespace StarWars.DataAccessGateway
{
    public class TerrenoRepositoryGateway : ITerrenoRepository
    {
        private readonly ITerrenoDapperRepository _dapper;
        private readonly ITerrenoEFRepository _entity;

        public TerrenoRepositoryGateway(ITerrenoDapperRepository dapper, ITerrenoEFRepository entity)
        {
            _dapper = dapper;
            _entity = entity;
        }

        public void Add(Terreno obj) => _entity.Add(obj);

        public void Dispose() => _entity.Dispose();

        public IEnumerable<Terreno> GetAll() => _entity.GetAll();

        public Terreno GetById(Guid id) => _entity.GetById(id);

        public void Remove(Terreno obj) => _entity.Remove(obj);

        public void Update(Terreno obj) => _entity.Update(obj);
    }
}