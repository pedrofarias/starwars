﻿using System;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using StarWars.Application.Application_Services_Interfaces;
using StarWars.Domain.Core.Domain_Entities;

namespace StarWars.API.Controllers
{
    [Route("Planeta")]
    [ApiController]
    public class PlanetaController : ControllerBase
    {
        private IPlanetaApplicationService _planetaApplicationService;
        private IClimaApplicationService _climaApplicationService;
        private ITerrenoApplicationService _terrenoApplicationService;

        public PlanetaController(
            IPlanetaApplicationService planetaApplicationService,
            IClimaApplicationService climaApplicationService,
            ITerrenoApplicationService terrenoApplicationService)
        {
            _planetaApplicationService = planetaApplicationService;
            _terrenoApplicationService = terrenoApplicationService;
            _climaApplicationService = climaApplicationService;
        }

        // GET: api/Planeta
        [HttpGet]
        public string Get()
        {
            var planetas = _planetaApplicationService.GetAll();
            return JsonConvert.SerializeObject(planetas);
        }

        // GET: api/Planeta/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(Guid id)
        {
            var planeta = _planetaApplicationService.GetById(id);
            return JsonConvert.SerializeObject(planeta);
        }

        // POST: api/Planeta
        [HttpPost]
        public void Post(Planeta planeta)
        {
            planeta.Clima = _climaApplicationService.GetById(planeta.Clima.Id);
            planeta.Terreno = _terrenoApplicationService.GetById(planeta.Terreno.Id);
            _planetaApplicationService.Add(planeta);
        }

        // PUT: api/Planeta/5
        [HttpPut("{id}")]
        public void Put(Guid id, [FromBody] Planeta planeta2)
        {
            var planeta = _planetaApplicationService.GetById(id);
            planeta.Nome = planeta2.Nome;
            planeta.Clima = planeta2.Clima.Id != null ? _climaApplicationService.GetById(planeta2.Clima.Id) : null;
            planeta.Terreno = planeta2.Terreno.Id != null ? _terrenoApplicationService.GetById(planeta2.Terreno.Id) : null;

            _planetaApplicationService.Update(planeta);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            var planeta = _planetaApplicationService.GetById(id);
            _planetaApplicationService.Remove(planeta);
        }

        [HttpGet("GetByName/{name}", Name = "GetByName")]
        public string GetByName(string name)
        {
            var planeta = _planetaApplicationService.GetByName(name);
            return JsonConvert.SerializeObject(planeta);
        }
    }
}
