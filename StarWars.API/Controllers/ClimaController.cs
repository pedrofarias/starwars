﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using StarWars.Application.Application_Services_Interfaces;
using StarWars.Domain.Core.Domain_Entities;

namespace StarWars.API.Controllers
{
    [Route("Clima")]
    [ApiController]
    public class ClimaController : ControllerBase
    {
        private readonly IClimaApplicationService _climaApplicationService;
        public ClimaController(IClimaApplicationService climaApplicationService)
        {
            _climaApplicationService = climaApplicationService;
        }

        // GET: api/Clima
        [HttpGet]
        public string Get()
        {
            var climas = _climaApplicationService.GetAll();
            return JsonConvert.SerializeObject(climas);
        }

        // GET: api/Clima/5
        [HttpGet("{id}", Name = "GetClima")]
        public string Get(Guid id)
        {
            var clima = _climaApplicationService.GetById(id);
            return JsonConvert.SerializeObject(clima);
        }

        // POST: api/Clima
        [HttpPost]
        public void Post(Clima clima)
        {
            _climaApplicationService.Add(clima);
        }

        // PUT: api/Clima/5
        [HttpPut("{id}")]
        public void Put(Guid id, [FromBody] Clima clima2)
        {
            var clima = _climaApplicationService.GetById(id);
            clima.Nome = clima2.Nome;
            _climaApplicationService.Update(clima);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            var clima = _climaApplicationService.GetById(id);
            _climaApplicationService.Remove(clima);
        }
    }
}
