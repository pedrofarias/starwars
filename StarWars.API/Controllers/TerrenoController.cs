﻿using System;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using StarWars.Application.Application_Services_Interfaces;
using StarWars.Domain.Core.Domain_Entities;

namespace StarWars.API.Controllers
{
    [Route("Terreno")]
    [ApiController]
    public class TerrenoController : ControllerBase
    {
        private readonly ITerrenoApplicationService _terrenoApplicationService;
        public TerrenoController(ITerrenoApplicationService terrenoApplicationService)
        {
            _terrenoApplicationService = terrenoApplicationService;
        }

        // GET: api/Terreno
        [HttpGet]
        public string Get()
        {
            var terrenos = _terrenoApplicationService.GetAll();
            return JsonConvert.SerializeObject(terrenos);
        }

        // GET: api/Terreno/5
        [HttpGet("{id}", Name = "GetTerreno")]
        public string Get(Guid id)
        {
            var terreno = _terrenoApplicationService.GetById(id);
            return JsonConvert.SerializeObject(terreno);
        }

        // POST: api/Terreno
        [HttpPost]
        public void Post(Terreno terreno)
        {
            _terrenoApplicationService.Add(terreno);
        }

        // PUT: api/Terreno/5
        [HttpPut("{id}")]
        public void Put(Guid id, [FromBody] Terreno terreno2)
        {
            var terreno = _terrenoApplicationService.GetById(id);
            terreno.Nome = terreno2.Nome;
            _terrenoApplicationService.Update(terreno);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            var terreno = _terrenoApplicationService.GetById(id);
            _terrenoApplicationService.Remove(terreno);
        }
    }
}
