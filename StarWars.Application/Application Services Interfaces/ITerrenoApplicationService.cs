﻿using StarWars.Domain.Core.Domain_Entities;

namespace StarWars.Application.Application_Services_Interfaces
{
    public interface ITerrenoApplicationService : IApplicationServiceBase<Terreno>
    {
    }
}
