﻿using StarWars.Domain.Core.Domain_Entities;

namespace StarWars.Application.Application_Services_Interfaces
{
    public interface IPlanetaApplicationService : IApplicationServiceBase<Planeta>
    {
        Planeta GetByName(string name);
    }
}
