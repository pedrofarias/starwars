﻿using StarWars.Application.Application_Services_Interfaces;
using StarWars.Domain.Core.Domain_Entities;
using StarWars.Domain.Domain_Services_Interfaces;

namespace StarWars.Application.Application_Services
{
    public class PlanetaApplicationService : ApplicationServiceBase<Planeta>, IPlanetaApplicationService
    {
        private readonly IPlanetaService _planetaService;
        public PlanetaApplicationService(IPlanetaService planetaService) : base(planetaService)
        {
            _planetaService = planetaService;
        }

        public Planeta GetByName(string name)
        {
            return _planetaService.GetByName(name);
        }
    }
}
