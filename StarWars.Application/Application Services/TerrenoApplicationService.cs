﻿using StarWars.Application.Application_Services_Interfaces;
using StarWars.Domain.Core.Domain_Entities;
using StarWars.Domain.Domain_Services_Interfaces;

namespace StarWars.Application.Application_Services
{
    public class TerrenoApplicationService : ApplicationServiceBase<Terreno>, ITerrenoApplicationService
    {
        private readonly ITerrenoService _terrenoService;
        public TerrenoApplicationService(ITerrenoService terrenoService) : base(terrenoService)
        {
            _terrenoService = terrenoService;
        }
    }
}
