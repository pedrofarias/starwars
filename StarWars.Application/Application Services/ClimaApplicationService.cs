﻿using StarWars.Application.Application_Services_Interfaces;
using StarWars.Domain.Core.Domain_Entities;
using StarWars.Domain.Domain_Services_Interfaces;

namespace StarWars.Application.Application_Services
{
    public class ClimaApplicationService : ApplicationServiceBase<Clima>, IClimaApplicationService
    {
        private readonly IClimaService _climaService;
        public ClimaApplicationService(IClimaService climaService) : base(climaService)
        {
            _climaService = climaService;
        }
    }
}
