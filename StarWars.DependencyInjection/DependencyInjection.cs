﻿using Microsoft.Extensions.DependencyInjection;
using StarWars.Application.Application_Services;
using StarWars.Application.Application_Services_Interfaces;
using StarWars.DataAccess.Dapper.Repositories;
using StarWars.DataAccess.EntityFramework.Interfaces;
using StarWars.DataAccess.EntityFramework.Repositories;
using StarWars.DataAccessGateway;
using StarWars.Domain.Core.Repository_Interfaces;
using StarWars.Domain.Domain_Services;
using StarWars.Domain.Domain_Services_Interfaces;

namespace StarWars.InversionOfControl
{
    public class DependencyInjection
    {
        public static void RegisterServices(IServiceCollection services)
        {
            #region Planeta
            services.AddScoped<IPlanetaApplicationService, PlanetaApplicationService>();
            services.AddScoped<IPlanetaService, PlanetaService>();
            services.AddScoped<IPlanetaRepository, PlanetaRepositoryGateway>();
            services.AddScoped<IPlanetaDapperRepository, PlanetaDapperRepository>();
            services.AddScoped<IPlanetaEFRepository, PlanetaEFRepository>();
            #endregion

            #region Clima
            services.AddScoped<IClimaApplicationService, ClimaApplicationService>();
            services.AddScoped<IClimaService, ClimaService>();
            services.AddScoped<IClimaRepository, ClimaRepositoryGateway>();
            services.AddScoped<IClimaDapperRepository, ClimaDapperRepository>();
            services.AddScoped<IClimaEFRepository, ClimaEFRepository>();
            #endregion

            #region Terreno
            services.AddScoped<ITerrenoApplicationService, TerrenoApplicationService>();
            services.AddScoped<ITerrenoService, TerrenoService>();
            services.AddScoped<ITerrenoRepository, TerrenoRepositoryGateway>();
            services.AddScoped<ITerrenoDapperRepository, TerrenoDapperRepository>();
            services.AddScoped<ITerrenoEFRepository, TerrenoEFRepository>();
            #endregion
        }
    }
}
